# Padrão de Documentação Bravo 2020 #

Este documento é um manual de especificação sobre o padrão de nomenclaturas utilizado no projeto Bravo 2020.

### Nomenclatura de Variaveis ###

| Tipo          | Abreviatura   | Nome Variavel |    Resultado    |
| ------------- |:-------------:| -------------:| ---------------:|
| string        |     str       |      Nome     |     strNome     |
| date          |      dt       |   Nascimento  |   dtNascimento  |
| integer       |     int       |    Unidades   |   intUnidades   |
| boolean       |      bl       |     Ativo     |     blAtivo     |
| float         |      fl       |     Numero    |     flNumero    |
| array         |      ar       | ListaUsuarios | arListaUsuarios |
| double        |      do       |   VlrDecimal  |   dlVlrDecimal  |
| object        |     obj       |    Usuario    |    objUsuario   |
| character     |     cht       |    Empresa    |    chtEmpresa   |
| datetime      |               |               |                 |
|               |               |               |                 |
|               |               |               |                 |


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


